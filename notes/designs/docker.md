docker
    run
        -d > detached
        -rm > remove container when close
        -p in:out > expose inner port to outer port 
        -it > interactive mode (not sure)

    container
        -ls
        -rm

    volume
        -ls
        -rm
        
    exec

    compose
        up
            --rm > remove container on exit
            -d > detached
            --build [service]? > rebuild image (all images if no <service>)
        down