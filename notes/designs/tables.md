Profile:id {
    username: string
    password: string
    verified: bool
}

Invite:inviteToken {
    timeStamp
}

Session:sessionToken {
    owner
    timeStamp
}

User:id {
    type [client, admin, agent]
    invites
    cellphone
    email
    banks []
    cryptoWallet
}

Bank:id {
    name
    accountNumber
    branch
}

Account:id {
    balance
    term
    interest
    condition
    type [current, freeze]
}

Transaction:id {
    amount
    sender
    receiver
    timestamp
    type [deposit, withdrawal, payment, loan, donation, freeze, unfreeze]
    method [account, cash, eft, cellphone, crypto]
}

Deposit_note:id {
    amount
    depositor
    validator
    timestamp
    status [open, closed, accepted, rejected, active, error]
    
}

Withdrawal_note:id {
    amount
    withdrawer
    validator
    timestamp
    status [open, close, accepted, rejected, active, error]
}

Credit_note:id {
    amount
    lender
    borrower
    timestamp
    interest
    installments
    term
    status [open, closed, active, settled, arrears, default, paused]
    expiry
    conditions
}

Conditions:Credit_note_id {
    defaultPenalty
    settlementReward
    maxMissedPayment
    missedPaymentPenalty
}