Client {
    Profile

    Account

    getLenders()
    getBorrowers()
    getContacts()
}

Admin {
    Profile
    
    Account

    mint()
    burn()

    invite()
    verify()
    exile()
}

Agent {
    Profile

    Account

    requestMint()
    requestBurn()

    requestInvite()
    requestVerify()
    requestExile()
}

Account {
    id
    balance
    type
    interest

    getBalance()
    getTransactions()

    deposit()
    withdraw()
    pay()
    lend()
    borrow()

    createDepositNote()
    createWithdrawalNote()
    createCreditNote()

    processDeposit()
    processWithdrawal()
    
    freeze()
    unfreeze()
}