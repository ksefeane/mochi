#todo

[linode console](https://cloud.linode.com/linodes)


# setup

- [x] create linux instance
- [x] ssh into it
	- [x] sudo dnf update -y
- [x] install git
	- [x] sudo dnf -y install git
	- [ ] 
- [x] install nvm
	- [x] curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash
	- [x] paste export
- [x] install node lts using nvm
- [x] use node lts
- [ ] clone code
	- [x] setup instance ssh access
	- [x] install yarn
	- [x] yarn install
- [ ] install docker
	- [ ] run docker
	- [ ] run db
- [ ] [[build]] app
# website

- [ ] create instance
- [ ] buy domain name

# instance

- [ ] 