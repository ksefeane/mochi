#todo #features

# /invite

---

## api

- [ ] load {
    - [ ] get user profile
    - [ ] check user type
        - [ ] not admin - redirect to
    - [ ] create invite token hash
   - [ ]  save token to db
    - [ ] return token (temporary)

---

## db

- [ ] Encryption.generateInviteTokenHash
- [ ] Invite.create

---


# /invite/[token]

---

## api 
- [ ] load {
    - [ ] get inviteToken url
    - [ ] if inviteToken in db
        - [ ] true - set inviteToken cookie
            - [ ]  redirect to /signin
        - [ ] false - redirect to /

---

## db
- [ ] Invite.search

---

