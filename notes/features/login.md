#todo  #features

## frontend

### form

- [ ] enter username
- [ ] enter password

------

## api

- [ ] form action 
    - [ ] check profile /
    - [ ] compare password hash /
    - [ ] create session token hash /
    - [ ] save session token to db /
    - [ ] send session token cookie /

---

## db

- [ ] Encryption.comparePasswordHash /
- [ ] Encryption.generateSessionTokenHash /
- [ ] Session.create /

---

