#todo #features 


## frontend

- [ ] create user form
    - [ ] type (linked invitationType) (todo) (admin, agent, client)
    - [ ] cellphone []
    - [ ] bank []
    - [ ] cryptowallet []
    - [ ] password
    - [ ] save button
- [ ] update info form
    - [ ] username
    - [ ] add/remove cellphone []
    - [ ] add/remove bank []
    - [ ] add/remove cryptowallet []
    - [ ] password
    - [ ] save button

---

## api

- [ ] load {
    - [ ] get profile
    - [ ] get user
        - [ ] false - create user
    - [ ] return info
- [ ] create user form action {
    - [ ] validate info
    - [ ] verify password
    - [ ] create user
    - [ ] save to db
- [ ] update info form action {
    - [ ] validate info
    - [ ] verify password
    - [ ] update profile
    - [ ] update user

---

## db

- [ ] Profile.update
- [ ] User.create
- [ ] User.update
- [ ] Invite.search