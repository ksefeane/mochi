#closed #features

# description

- [x] use hooks for route protection

---
## api (hooks)

- [x] handle
	- [x] check is route valid
		- [x] else redirect /login
	- [x] authenticate session cookie (Auth.sessionCookie)
		- [x] get sessionToken cookie
		- [x] if sessionToken in db
			- [x] false - delete sessionToken cookie
			- [x] true - return profile
	- [x] check user valid for user route
		- [x] else redirect /login
	- [x] check user valid for admin route
		- [x] else redirect /login


------
## functions

- [x] Auth.sessionCookie

---
## db

- [x]  Session.search /

---