#todo #features


## frontend

- [ ] create username
- [ ] create password

---

## api

- [ ] load
	- [ ] get inviteToken cookie
	- [ ] if not found redirect to /
	- [ ] check if invite token exists in db
	- [ ] if not found redirect to /
- [ ] form action
	- [ ] validate username
	- [ ] validate password
	- [ ] check if profile exists
		- [ ] if not found return fail message
	- [ ] create profile
	- [ ] create hash password
	- [ ] validate password
	- [ ] encrypt password
	- [ ] save to db
	- [ ] delete invite from db
	- [ ] redirect to login

---

db

- [ ] Profile.create /
- [ ] Profile.fetch /
- [ ] Encryption.generatePasswordHash /
- [ ] Invite.delete

---