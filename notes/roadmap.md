#open

# features

- [ ] [[signin]]
- [ ] [[login]]
- [ ] [[nav]]
- [x] [[session]]
- [ ] [[invite]]
- [ ] [[logout]]
- [ ] [[profile]]
- [ ] [[user]]

# hosting

- [ ] [[docker]]
- [ ] [[linode]]

# styling

- [ ] [[tailwind]]
- [ ] darkmode
- [x] [[terminal colors]]
- [ ] 

# ci cd

- [ ] merge
- [ ] [[build]]
- [ ] deploy