#!/bin/bash

USER=root
PASS=katakuri

docker run --rm --pull always -p 8000:8000 surrealdb/surrealdb:latest start file:///var/lib/surrealdb.db --user $USER --pass $PASS