// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
type UserType = 'client' | 'agent' | 'admin';

declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			user: {
				username: string;
				type: UserType;
			};
		}
		// interface PageData {}
		// interface Platform {}
	}
}

export {};
