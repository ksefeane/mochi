import { Query } from '$lib/server/classes/Query';
import { redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
import { Logger } from '$lib/server/classes/Logger';

export const actions: Actions = {
	logout: async ({ cookies }) => {
		const sessionTokenCookie = cookies.get('sessionToken');

		Logger.init('/', 'actions(logout)');

		if (!sessionTokenCookie) {
			Logger.log('sessionTokenCookie not found\n - do nothing');
			return;
		}
		const sessionTokenDb = await Query.Session.search(sessionTokenCookie);

		if (!sessionTokenDb.data) {
			Logger.log('sessionTokenDb not found (?)');
		}

		await Query.Session.delete(sessionTokenCookie);
		cookies.delete('sessionToken');

		Logger.log(
			`login session deleted\n - sessionTokenDb deleted\n - sessionTokenCookie deleted\n - redirect to /login`
		);
		throw redirect(303, '/login');
	}
};
