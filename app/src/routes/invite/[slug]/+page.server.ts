import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { Query } from '$lib/server/classes/Query';
import { Logger } from '$lib/server/classes/Logger';

export const load: PageServerLoad = async ({ params, cookies }) => {
	const inviteTokenSlug = params.slug;

	Logger.init(`/invite/${inviteTokenSlug}`, 'load');

	const inviteTokenDb = await Query.Invite.search(inviteTokenSlug);

	if (!inviteTokenDb.data) {
		Logger.log(`inviteTokenDb not found\n - redirect to /login`);
		throw redirect(303, '/login');
	}

	cookies.set('inviteToken', inviteTokenSlug, {
		path: '/'
	});

	Logger.log(`inviteTokenDb found\n - set cookie: ${inviteTokenSlug}\n - redirect to /signin`);

	throw redirect(303, '/signin');
};
