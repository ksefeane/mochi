import { Query } from '$lib/server/classes/Query';
import { RANDOM_NUMBER_RANGE } from '$lib/server/types/constants';
import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { Logger } from '$lib/server/classes/Logger';

export const load: PageServerLoad = async () => {
	const timeStamp = new Date().toLocaleString();
	const randomNumber = Math.random() * RANDOM_NUMBER_RANGE;
	const inviteTokenHashDb = await Query.Encryption.generateInviteTokenHash(
		timeStamp,
		randomNumber.toString()
	);

	Logger.init(`/invite`, `load`);
	await Query.Invite.create(inviteTokenHashDb.data, 'admin');

	Logger.log(`inviteTokenDb created`);
	return { inviteToken: inviteTokenHashDb.data };
};
