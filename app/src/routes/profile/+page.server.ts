import { Logger } from '$lib/server/classes/Logger';
import { Query } from '$lib/server/classes/Query';
import type { Actions, PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ cookies }) => {
	const sessionTokenCookie = cookies.get('sessionToken');

	Logger.init('/profile', 'load');

	if (!sessionTokenCookie) {
		Logger.log('sessionTokenCookie not found');
		return;
	}

	const sessionDb = await Query.Session.search(sessionTokenCookie);
	if (!sessionDb.data) {
		Logger.log('sessionDb not found');
		return;
	}

	const userDb = await Query.User.fetch(sessionDb.data.profile.id);

	if (!userDb.data) {
		Logger.log('userDb not found');
		return;
	}

	console.log(userDb.data);

	return {
		user: userDb.data
	};
};

export const actions: Actions = {
	update: async ({ request, cookies }) => {
		const sessionTokenCookie = cookies.get('sessionToken');
		if (!sessionTokenCookie) return;

		const formData = await request.formData();

		const cellphone = formData.get('cellphone') as string;
		const bank = formData.get('bank') as string;
		const cryptoWallet = formData.get('cryptoWallet') as string;

		Logger.init('/profile', 'actions(update)');

		const sessionDb = await Query.Session.search(sessionTokenCookie);

		let userDb = await Query.User.fetch(sessionDb.data.profile.id);

		const user = userDb.data;

		user.cellphone = cellphone ?? user.cellphone;
		user.banks = bank ? [...(user.banks ?? []), bank] : user.banks;
		user.cryptoWallets = cryptoWallet
			? [...(user.cryptoWallets ?? []), cryptoWallet]
			: user.cryptoWallets;

		userDb = await Query.User.update(sessionDb.data.profile.id, user);
	}
};
