import { Query } from '$lib/server/classes/Query';
import { redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
import { RANDOM_NUMBER_RANGE } from '$lib/server/types/constants';
import { Logger } from '$lib/server/classes/Logger';

export const actions: Actions = {
	default: async ({ request, cookies }) => {
		const formData = await request.formData();
		const username = formData.get('username') as string;
		const password = formData.get('password') as string;

		Logger.init('/login', 'actions(default)');

		if (!(username && password)) {
			Logger.log(`invalid username/password\n - return (please enter...)`);
			return {
				success: false,
				message: 'please enter username and password'
			};
		}

		const profileDb = await Query.Profile.fetch(username);
		if (!profileDb?.data) {
			Logger.log(`profileDb not found\n - return (invalid username/password)`);
			return {
				success: false,
				message: 'invalid username'
			};
		}

		const passwordDb = await Query.Encryption.comparePasswordHash(
			password,
			profileDb.data.password
		);

		if (!passwordDb?.data) {
			Logger.log(`passwordDb not valid\n - return (invalid username/password)`);
			return {
				success: false,
				message: 'invalid password'
			};
		}
		const randomNumber = Math.random() * RANDOM_NUMBER_RANGE;
		const sessionTokenHashDb = await Query.Encryption.generateSessionTokenHash(
			profileDb.data.id,
			randomNumber.toString()
		);

		await Query.Session.create(profileDb.data.id, sessionTokenHashDb.data);

		cookies.set('sessionToken', sessionTokenHashDb.data);

		Logger.log(
			`login session created\n - sessionTokenDb created\n - sessionTokenCookie created\n - redirect to  /`
		);
		throw redirect(303, '/');
	}
};
