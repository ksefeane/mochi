import { Query } from '$lib/server/classes/Query';
import { redirect } from '@sveltejs/kit';
import type { Actions, PageServerLoad } from './$types';
import { Logger } from '$lib/server/classes/Logger';

export const load: PageServerLoad = async ({ cookies }) => {
	const inviteTokenCookie = cookies.get('inviteToken');

	Logger.init('/signin', 'load');

	if (!inviteTokenCookie) {
		Logger.log(`inviteTokenCookie not found\n - redirect to /login`);
		throw redirect(303, '/login');
	}

	const inviteTokenDb = await Query.Invite.search(inviteTokenCookie);

	if (!inviteTokenDb.data) {
		Logger.log(`inviteTokenDb not found\n - redirect to /login`);
		throw redirect(303, '/login');
	}

	Logger.log(`inviteTokenDb found`);
};

export const actions: Actions = {
	default: async ({ request, cookies }) => {
		const inviteTokenCookie = cookies.get('inviteToken');
		const formData = await request.formData();
		const username = formData.get('username') as string;
		const password = formData.get('password') as string;

		Logger.init('/signin', 'actions(default)');

		if (!inviteTokenCookie) {
			Logger.log(`inviteTokenCookie not found\n - redirect to /login`);
			throw redirect(303, '/login');
		}

		if (!(username && password)) {
			Logger.log(`invalid username/password\n - return (please enter...)`);
			return {
				success: false,
				message: 'please enter username and password'
			};
		}

		let profileDb = await Query.Profile.fetch(username);
		if (profileDb.data) {
			Logger.log(`profile found\n - return (username unavailable)`);
			return {
				success: false,
				message: 'username unavailable'
			};
		}
		const passwordHashResponse = await Query.Encryption.generatePasswordHash(password);

		if (!passwordHashResponse.data) {
			Logger.log(`failed to generate password hash\n - return (password error)`);
			return {
				success: false,
				message: 'password error'
			};
		}

		profileDb = await Query.Profile.create({
			username,
			password: passwordHashResponse.data as string
		});
		if (profileDb.status !== 'OK') {
			Logger.log(`failed to create profile\n - return (account error)`);
			return {
				success: false,
				message: 'account error'
			};
		}

		console.log(profileDb);

		await Query.User.create({ type: 'admin' }, profileDb.data.id);

		await Query.Invite.delete(inviteTokenCookie);
		cookies.delete('inviteTokenCookie', { path: '/' });

		Logger.log(
			`signin successful\n - create user\n - delete inviteTokenDb\n - delete inviteTokenCookie\n - redirect to /login`
		);
		throw redirect(303, '/login');
	}
};
