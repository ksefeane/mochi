import type { AccountType, UserType } from './types';

export interface Profile {
	id?: string;
	username: string;
	password?: string;
	token?: string;
}

export interface User {
	type?: UserType;
	invites?: number;
	cellphone?: string;
	email?: string;
	banks?: string[];
	cryptoWallets?: string[];
}

export interface Account {
	balance: number;
	type: AccountType;
}
