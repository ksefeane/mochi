export type UserType = 'admin' | 'agent' | 'client';

export type AccountType = 'current' | 'freeze';

export type RouteType = 'user' | 'admin' | 'invite' | 'public';
