export const RANDOM_NUMBER_RANGE = 100_000;

export const PUBLIC_ROUTES = ['login'];

export const INVITE_ROUTES = ['signin'];

export const USER_ROUTES = [
	'',
	'profile',
	'account',
	'notes',
	'pay',
	'lend',
	'borrow',
	'deposit',
	'withdraw'
];

export const ADMIN_ROUTES = ['invite'];
