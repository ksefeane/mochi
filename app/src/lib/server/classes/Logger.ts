export class Logger {
	private static _path?: string;
	private static _serverFunction?: string;

	static init = (path?: string, serverFunction?: string) => {
		this._path = path;
		this._serverFunction = serverFunction;
		return this;
	};

	static log = async (action: any) => {
		const path = this._path ? `${this._path} > ` : '';
		const serverFunction = this._serverFunction ? `${this._serverFunction}: ` : '';
		console.log('\x1b[37m' + path + serverFunction, action);
		return this;
	};

	static error = async (data: any) => {
		const path = this._path ? `${this._path} > ` : '';
		const serverFunction = this._serverFunction ? `${this._serverFunction}: ` : '';
		console.error('\x1b[31m' + path + serverFunction, data);
		return this;
	};
}
