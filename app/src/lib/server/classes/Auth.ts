import type { Cookies } from '@sveltejs/kit';
import { Query } from './Query';

export class Auth {
	static sessionCookie = async (cookies: Cookies) => {
		const sessionCookie = cookies.get('sessionToken');

		if (!sessionCookie) throw 'sessionCookie not found';

		const sessionDb = await Query.Session.search(sessionCookie);
		if (!sessionDb.data) {
			cookies.delete('sessionToken');
			throw 'sessionDb not found';
		}
		return sessionDb.data.profile;
	};
}
