import { EncryptionQuery } from './Query/EncryptionQuery';
import { InviteQuery } from './Query/InviteQuery';
import { ProfileQuery } from './Query/ProfileQuery';
import { SessionQuery } from './Query/SessionQuery';
import { UserQuery } from './Query/UserQuery';

export class Query {
	static Profile = ProfileQuery;
	static User = UserQuery;
	static Encryption = EncryptionQuery;
	static Session = SessionQuery;
	static Invite = InviteQuery;
}
