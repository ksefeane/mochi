import { host, login, port } from '../configs/db_configs';

export class DB {
	private static _user = login.user;
	private static _pass = login.pass;
	private static _namespace = login.namespace;
	private static _db = login.db;
	private static _url = `http://${host}:${port}/sql`;

	private static get authHeaderBase64() {
		return `Basic ${btoa(`${this._user}:${this._pass}`)}`;
	}

	private static get headers() {
		return new Headers({
			Authorization: this.authHeaderBase64,
			Accept: 'application/json',
			NS: this._namespace,
			DB: this._db
		});
	}

	static query = async (sqlString: BodyInit) => {
		console.log('\x1b[37m' + '🟢', sqlString);

		const response = await fetch(this._url, {
			method: 'POST',
			headers: this.headers,
			body: sqlString
		});
		const data = await response.json();
		const res = data[0];

		return {
			status: res.status,
			time: res.time,
			data: res.result?.[0],
			errorMessage: res?.detail
		};
	};
}
