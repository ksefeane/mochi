import { DB } from '../DB';

export class SessionQuery {
	static create = async (profile: string, sessionToken: string) =>
		await DB.query(
			`CREATE session:${sessionToken} SET profile = ${profile}, timeStamp = time::now()`
		);

	static search = async (sessionToken: string) =>
		await DB.query(`SELECT profile.username, profile.id FROM session:${sessionToken}`);

	static delete = async (sessionToken: string) => await DB.query(`DELETE session:${sessionToken}`);
}
