import type { UserType } from '$lib/server/types/types';
import { DB } from '../DB';

export class InviteQuery {
	static create = async (inviteToken: string, type: UserType) =>
		await DB.query(`CREATE invite:${inviteToken} SET timeStamp = time::now(), type = '${type}'`);

	static search = async (inviteToken: string) =>
		await DB.query(`SELECT * FROM invite:${inviteToken}`);

	static delete = async (inviteToken: string) => await DB.query(`DELETE invite:${inviteToken}`);
}
