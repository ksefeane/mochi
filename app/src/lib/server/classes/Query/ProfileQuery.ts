import type { Profile } from '$lib/server/types/interfaces';
import { DB } from '../DB';

export class ProfileQuery {
	static create = async ({ username, password }: Profile) =>
		await DB.query(`CREATE profile CONTENT {
            username: '${username}',
            password: '${password}'
        }`);

	static fetch = async (username: string) =>
		await DB.query(`SELECT * FROM profile WHERE username = '${username}'`);
}
