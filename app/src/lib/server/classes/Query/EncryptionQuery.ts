import { DB } from '../DB';

export class EncryptionQuery {
	static generatePasswordHash = async (password: string) =>
		await DB.query(`SELECT * FROM crypto::argon2::generate("${password}")`);

	static comparePasswordHash = async (password: string, hash: string) =>
		await DB.query(`SELECT * FROM crypto::argon2::compare("${hash}", "${password}")`);

	static generateSessionTokenHash = async (profileId: string, randomNumberString: string) =>
		await DB.query(`SELECT * FROM crypto::sha512("${profileId + randomNumberString}")`);

	static generateInviteTokenHash = async (timestamp: string, randomNumberString: string) =>
		await DB.query(`SELECT * FROM crypto::sha256("${timestamp + randomNumberString}")`);
}
