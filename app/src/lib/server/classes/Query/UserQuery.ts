import type { User } from '$lib/server/types/interfaces';
import { DB } from '../DB';

export class UserQuery {
	static create = async ({ type }: User, profileId: string) =>
		await DB.query(`CREATE user SET type = '${type}', profile = ${profileId}`);

	static fetch = async (profileId: string) =>
		await DB.query(`SELECT * FROM user WHERE profile = ${profileId}`);

	static update = async (profileId: string, user: User) => {
		return await DB.query(`UPDATE user MERGE ${JSON.stringify(user)} WHERE profile = ${profileId}`);
	};
}
