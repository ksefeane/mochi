import { ADMIN_ROUTES, INVITE_ROUTES, PUBLIC_ROUTES, USER_ROUTES } from '../types/constants';
import type { RouteType } from '../types/types';

export class Route {
	private static _pathName?: string;

	static get pathName(): string | undefined {
		return this._pathName;
	}

	static set pathName(newPathName: string) {
		this._pathName = newPathName;
	}

	static get isValid() {
		return this.verify([...USER_ROUTES, ...ADMIN_ROUTES, ...INVITE_ROUTES, ...PUBLIC_ROUTES]);
	}

	private static verify = (routes: string[]) =>
		routes.some((route) => route === this.pathName?.split('/')[1]);

	static isType = (type: RouteType) => {
		switch (type) {
			case 'user':
				return this.verify(USER_ROUTES);
			case 'admin':
				return this.verify(ADMIN_ROUTES);
			case 'invite':
				return this.verify(INVITE_ROUTES);
			case 'public':
				return this.verify(PUBLIC_ROUTES);
		}
	};
}
