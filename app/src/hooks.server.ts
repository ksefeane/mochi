import { Auth } from '$lib/server/classes/Auth';
import { Logger } from '$lib/server/classes/Logger';
import { Route } from '$lib/server/classes/Route';
import { redirect, type Handle } from '@sveltejs/kit';

export const handle: Handle = async ({ event, resolve }) => {
	const { url, cookies, locals } = event;
	Logger.init('hooks', 'handle');
	Logger.log('@ ' + url.pathname);
	Route.pathName = url.pathname;

	if (!Route.isValid) {
		Logger.error('invalid route');
		throw redirect(303, '/login');
	}

	locals.user = await Auth.sessionCookie(cookies).catch((e) => {
		Logger.error(e);
	});

	if (Route.isType('user') && !locals.user) {
		Logger.error('unauthorized user access\n - redirect to /login');
		throw redirect(303, '/login');
	}
	if (Route.isType('admin') && (!locals.user || locals.user.type !== 'admin')) {
		Logger.error('unauthorized admin access\n - redirect to /login');
		// TODO create admin user
		// throw redirect(303, "/login");
	}
	if (locals.user && Route.isType('public')) {
		Logger.log('user already logged in\n redirect to /');
		throw redirect(303, '/');
	}

	return await resolve(event);
};
